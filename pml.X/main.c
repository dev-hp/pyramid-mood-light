/*
 * File:   main.c
 * Author: Hagen Patzke
 *
 * 2020-06-28 first version
 * 2021-02-25 cleanup, add config bits
 */

#include <xc.h>

// Device: PIC12F1572
// device configuration
// CONFIG1
#pragma config FOSC = INTOSC  // ->INTOSC oscillator; I/O function on CLKIN pin
#pragma config WDTE = OFF     // Watchdog Timer Enable->WDT disabled
#pragma config PWRTE = OFF    // Power-up Timer Enable->PWRT disabled
#pragma config MCLRE = ON     // MCLR Pin Function Select->MCLR/VPP pin function is MCLR
#pragma config CP = OFF       // Flash Program Memory Code Protection->Program memory code protection is disabled
#pragma config BOREN = ON     // Brown-out Reset Enable->Brown-out Reset enabled
#pragma config CLKOUTEN = OFF // Clock Out Enable->CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin

// CONFIG2
#pragma config WRT = OFF     // Flash Memory Self-Write Protection->Write protection off
#pragma config PLLEN = OFF   // PLL Enable->4x PLL disabled
#pragma config STVREN = ON   // Stack Overflow/Underflow Reset Enable->Stack Overflow or Underflow will cause a Reset
#pragma config BORV = LO     // Brown-out Reset Voltage Selection->Brown-out Reset Voltage (Vbor), low trip point selected.
#pragma config LPBOREN = OFF // Low Power Brown-out Reset enable bit->LPBOR is disabled
#pragma config LVP = ON      // Low-Voltage Programming Enable->Low-voltage programming enabled

#define _XTAL_FREQ 4000000 // needed for _delay functions

void setup() {
    OSCCON = 0b01101010; // Internal Oscillator, 4 MHz HF, no PLL
    APFCON = 0b10000100; // alternative pin configuration: RX/TX on RA5/4
    ANSELA = 0b00000000; // no analog inputs
    TRISA = 0b11111000; // RA0/1/2 output, all else input
    WPUA = 0b00000000; // No weak pull-ups (no input)
    INLVLA = 0b00000000; // All inputs TTL level
    ODCONA = 0b00000000; // Open Drain Control: all outputs are push-pull
    SLRCONA = 0b00000000; // Slew-rate as fast as possible
    LATA = 0b00000000; // all output latches low
}

void cycle() {
#define ONESHOT 302 // 302 = 2 seconds

    unsigned int inner;
    unsigned int outer;

    while (1) {
        unsigned long pattern = 0x07563421;
        for (outer = 0; outer < 8; outer++) {
            for (inner = 0; inner < ONESHOT; inner++) {
                // 6.6 ms period, 33% duty cycle
                PORTA = (PORTA & 0xF8) | (pattern & 0x07);
                __delay_us(2200);
                PORTA = (PORTA & 0xF8);
                __delay_us(4400);
            }
            pattern >>= 4;
        }
    }
}

void main(void) {
    // default OSC after reset is 500kHz
    setup();
    // Now we should have 4MHz
    cycle();
}
