# Pyramid Mood Light

## Overview

The local ['Big Bazaar'](https://www.bigbazar.eu/) in Utrecht (Haroekoeplein) had a "Pyramid Mood Light" from PartyFunLights.com for about 8 EUR.

![Image of the Pyramid Mood Light](image/moodlight-shot-triple-w600.jpg)

Here is a [YouTube Video of the device](https://www.youtube.com/watch?v=35l62uLPz3o).
Conrad Electronics also has [a product page for it](https://www.conrad.com/p/led-monochrome-party-light-effect-multi-colour-no-of-bulbs-3-753099).

In the box is the Pyramid Mood Light itself, and a 'Leonone' 4.5V 500mA power supply.

My original intention was to use this as a sort of TV backlight.

It's not very bright, however, and it cycles light only in a fixed pattern:

1. green + fade-in red
2. fade-out green + red
3. red + fade-in blue
4. fade-out red + blue
5. blue + fade-in green
6. fade-out blue + green

Hm. So the light is never white (all three LEDs on). And it also never stays on one single, pure color.

Never mind - the outer casing alone is worth the money, and I figured I could always replace the internal circuit and lights with (e.g.) a white LED strip.

But before replacing anything, I wanted to know what's inside. Maybe I can modify the device to better suit my needs?

## Open device

The bottom plate is attached with four black Philips screws.
The diffusor is attached with glue, but luckily it's not very strong, so I could remove it (carefully) without any damage.

This is how it looks without diffusor on the top (I removed the two metal screws which hold the circuit board in place):

![Device with diffusor removed](image/pcb-top-case-w600.jpg)

On top is the solder side of the [PCB](https://en.wikipedia.org/wiki/Printed_circuit_board) - you can see the three LEDs.
It looks like this has been in a kind of solder 'bath', and not cleaned afterwards (the brown stains are flux).

Here is a look at the component side:

![Component side of the PCB visible](image/pcb-top-case-components-w600.jpg)

You can see the 'foot' of the device is mostly empty, except the power connector.
The black plastic base is shaped in a quite complex pattern inside, maybe this base is also used for other, more complex devices.

## Circuit diagram

As it is a single-sided circuit board with only a handful of components, tracing the circuit was not difficult:

![Mood Light circuit diagram](circuit/PartyFunLights-com-71.975-w800.png)

## Bill Of Materials

| Label  |            Value | Description                                                   |
| ------ | ---------------: | ------------------------------------------------------------- |
| U1     |              ??? | 8-pin DIP, Unknown Integrated Circuit                         |
| Q1,2,3 |            C2655 | NPN Transistor                                                |
| ZD1    |             C4V7 | Zener diode 4.7 Volt                                          |
| R      |              ??? | Red power LED                                                 |
| B      |              ??? | Blue power LED                                                |
| G      |              ??? | Green power LED                                               |
| R1,2,3 |      820 &Omega; | resistors between MCU pins 7,6,5 and base of Q1,2,3           |
| R4     |       24 &Omega; | current-limiting resistor for red LED                         |
| R5     |       18 &Omega; | current-limiting resistor for blue LED                        |
| R6     |       18 &Omega; | current-limiting resistor for green LED                       |
| R7     |      150 &Omega; | resistor to limit current for MCU power supply                |
| R8     |      10 k&Omega; | resistor parallel to ZD1 to provide a controlled minimum load |
| C1     | 220 &micro;F/16V | capacitor to stabilize power supply input                     |
| C2     | 100 &micro;F/10V | capacitor to stabilize MCU power (and filter transients)      |

_The labels are printed on the circuit board._

There is also a print "NO-258" on the PCB, and space for four components D1..D4 which are not soldered (a discrete bridge rectifier, for an AC input option).

## Measuring duty cycle and output voltage

To see how the unknown IC controls the LED brightness, I connected a small Vellemann HPS140i (Digital Signal Oscilloscope) to the resistor at IC pin 7 (red).

There are no big surprises here - we can see a PWM (Pulse Width Modulated) output.

Fade-in of the LED (when it's getting brighter) takes ca. 2 seconds from 'off' (everything low) to 'maximum brightness' (everything high).

Checking with blue and green oputputs, they looked identical to the red one (albeit starting fade-in and fade-put at different times).

Using the 'hold' feature, I could measure ~~duty cycle~~ period and output voltage of the PWM signal:

| ![Period measured on red LED output](image/moodlight-red-dutycycle-w400.jpg) |     | ![Output voltage measured on red LED output](image/moodlight-red-voltage-w400.jpg) |
| ---------------------------------------------------------------------------- | --- | ---------------------------------------------------------------------------------- |
| Period: 6.6ms                                                                |     | Output voltage: 3.3V (ca.)                                                         |

A period of 6.6ms means the LED is pulsed with ca. 150 Hz, making it flicker-free. Nice!

The output voltage of 3.3V is common for I/O ports of CMOS ICs.

## U1 considerations

My gut feeling was that the 8-pin [IC](https://en.wikipedia.org/wiki/Integrated_circuit) U1 is a sort of microcontroller.

- An Application-Specific Integrated Circuit (ASIC) would only make sense if this device was produced in very high volumes,
  but even then this would probably be considered too expensive for a relatively simple and cheap LED mood light.

  So let's look at the smallest and cheapest MCUs (Micro-Controller Units) suitable for such a task.
  Not much processing power is needed, so it's likely an 8-bit controller.

  Easily available 8-bit MCUs in Europe are

  - Atmel AVR (Advanced Versatile RISC) 'ATtiny' (or 'TinyAVR') family
  - Microchip PIC (Peripheral Interface Controller) family

- Looking at the specifications of the ATtiny and PIC series of 8-bit and 8-pin microcontrollers,
  specifically the location of 'power' and 'ground'pins (VCC and GND),
  it looks like the designers used an 8-bit PIC.

- 2020-07-14: Yes, it could be a Microchip PIC, but it could also be an MCU from
  [Padauk](http://www.padauk.com.tw/en/product/index.aspx?kind=41).
  And because these chips are much, much cheaper than PICs, and probably easier to get if you manufacture in China, it probably is one of them.

### Is it a PIC ? If so, which one ?

For me, a PIC is a tiny bit of a pity, because I already have an ATtiny12 and ATtiny15L here at home. It is also an opportunity to delve into PICs in my own little hobby project.

But the Atmel ATtiny VCC and GND pin location follows the traditional '7400' pattern: they are diagonally opposite each other.

As you can see in the schematic above, VCC and GND of our 'unknown' IC are on the same side - at the 'bottom' of the IC is one pin not connected, and the other one is an output.

So it looks like we have a sort of PIC device here, with

- three of six output ports used to drive the LEDs,
- one port connected to ground (GND)
  - ~~maybe that's a RESET or programming input~~ 2020-06-19: no, -MCLR would be pin 4, not pin 3. See below. :-)
  - leaving it 'open' could invite all sort of unwanted behavior, if some outside interference is interpreted as an input signal,
- two ports left unconnected (NC).

These small microcontrollers also often come with a built-in oscillator - great for application that are not timing-critical, and that are price-sensitive (=they should use as few components as possible).

- 2020-06-19: at the moment I think it could be a PIC12F508 device:  
  ![PIC12F508 pinout](image/pic12f508-pinout.png)

- 2020-07-14: at the moment I think it could be a [Padauk PMS152 device](http://www.padauk.com.tw/upload/doc/PMS152_datasheet_v105_EN_20200609.pdf).

### Red Herrings

Looking at the specifications (again), one of the best matching PIC devices for controlling PWM output to drive an RGB LED is the PIC12F157x.

PIC12F1571 and PIC12F1572 only differ in the amount of available program memory,
but they have four 16-bit counters that can be used to control Generic Programmable Input and Output pins (GPIO pins) with a very simple program.

On the [Microchip website](https://www.microchip.com/) are even an application note and a promotion page for using this chip for LED color mixing:

- [AN1562 - High Resolution RGB LED Color Mixing Application Note](http://ww1.microchip.com/downloads/en/AppNotes/00001562B.pdf)

- [RGB badge color mixing demonstration](https://www.microchip.com/promo/rgb-badge-color-mixing-demo) page.

- 2020-06-19: a PIC12F1572 is really well suited for my own experiments for LED control (that's why Microchip used it for the RGB badge), but a LED lamp manufacturer would choose a much simpler device that is just 'good enough' to fulfil the needs of the design.

---

## To be continued

Even if the IC used for driving the three LEDs is not a PIC12F157x, but some other controller (maybe there is an original Chinese microcontroller design that just happens to have this pinout), I can replace U1 with one, and program it with an algorithm of my own.

- 2020-06-19: confirmed, the PIC12F1572 I ordered should be suitable for the task (good that I did not go for the LF variant, this one only works with a maximum VDD voltage of 3.6V, the 'F' variant is rated up to 5.5V)

If I am really super-lucky, and if it is a PIC12, maybe I can even read out and re-program the existing chip.

- 2020-06-16: I tried reading it with a [TL866II](http://autoelectric.cn/EN/TL866_main.html), but this seemed not to work - ~~but then the TL866II does not officially support PIC12 devices~~.)
- 2020-06-19: Wrong, the TL866-II does officially support the PIC12F508, need to give it another go.

In the past, I worked on hobby projects with Atmel AVRs, so I'm more than happy to learn more about the Microchip PIC platform.

- 2020-06-28: Replaced the unknown IC with a programmed PIC12F1572 and verified that it works ('manual' PWM, just to see if it works). Yay! Before I check it in, I need to clean up and 'minimize' the source code.

- 2020-07-01: Code check-in will have to wait a little longer - today I received my 'Omdazz' Cyclone-IV FPGA development board, and I need to verify that it works. :-)

- 2020-07-15: I received my 'Lychee Tang Primer' Anlogic FPGA development board. This one needs to be tested, too.

- 2020-07-15: The MCU is very likely not a PIC. China has lots of (much cheaper) microcontrollers with a matching pinout.

---

## Timeline

- 2020-06-12: Start of 'Pyramid Mood Light' project. Take device apart, make a few photos.

- 2020-06-13:

  - Circuit analysis and research.

  - When I follow the AN1562 link to the RGB badge page on the Microchip web site,
    I get redirected to a 404 (page not found) error page with a discount offer for an MPLAB Snap programmer.

    Such a programmer, plus a few PIC devices, would be perfect to improve my 'Pyramid Mood Light' and to learn more about PICs.

    Unfortunately, the discount coupon code does not work, so I contact Microchip support.

- 2020-06-14: Contact Conrad Electronics to ask if I am allowed use their product photo (it's really nice) for this project page.

- 2020-06-15:

  - In a very friendly mail, Conrad tells me they unfortunately don't have the rights of the photo.
    (I only see this mail one day later - GMail is sometimes a bit too helpful keeping my inbox clean.)
    OK, so I need to make a photo myself.

- 2020-06-16:

  - Create project page on GitLab and publish it.

  - Microchip support responds to my inquiry and sends me a new discount code.
    This one works, I place an order, and now I'm looking forward to an MPLAB Snap programmer and five PIC12F1572 devices. :-)

- 2020-06-18:

  - MPLAB Snap programmer delivered. Yay, that ws fast!

  - Unfortunately I needed to [watch a YouTube video (!) to learn that the MPLAB Snap does not support High-Voltage programming (HVP)](https://youtu.be/aAAurahPSbg?t=156).
    And without HVP, you lose a pin to MCLR (which is also a RESET input).

    You definitely want to have a programmer that can HVP when you work with low-pincount devices.
    Looks like [another company who made a PIC programmer also offers an a adapter from LVP to HVP and published the circuit for it](https://www.northernsoftware.com/nsdsp/hvp.htm).

    **Beats me why Microchip does not provide a comparison chart of their development tools.** Not good.

- 2020-06-19:

  - Formatted and sorted the exported CSV list of all Microchip 8-bit MCUs.

  - Looks like the 'Pyramid Mood Light' manufacturer might have used a PIC12F508:  
    ![PIC12F508 pinout](image/pic12f508-pinout.png)

    That would explain the relatively low PWM frequency. And it cannot be programmed with Low-Voltage Programming.

    Also, looking at the programming documentation, I learned you need to be super-careful which PIC device you have:

    - the PIC12F508 has VPP between 12.5V and 13.5V,
    - the PIC12F1572 has VPP between 8V and 9V (and a completely different memory layout).

    Unfortunately, there is no uniform way of reading PIC device IDs.

    - You have to read some data from memory (at least this data cannot be protected).
    - Depending on the device, the data location is different.
    - Older chips don't support LVP (low-voltage programming, at the normal operating voltage VDD).  
      If you have an unmarked device, you could 'fry' it when you attempt to read its device ID. Ugh.

  - Found a [blog entry with an Arduino PIC12 programmer that supports HV (high-voltage) programming at 13V](https://www.reenigne.org/blog/a-pic12-programmer-from-an-arduino/).  
    This was actually designed to program PIC12F508 devices, [the source code for it is quite interesting](https://github.com/reenigne/reenigne/tree/master/intervals/programmer).

- 2020-06-23: Microchip parcel from Thailand arrived with my order of five PIC12F1572. :-)

- 2020-06-28: Replaced the unknown IC with a programmed PIC12F1572 and verified that it works ('manual' PWM, just to see if it works).
  Now I know why there is no 'white' period in the light colour sequence they use - the LEDs do not have the same brightness,
  and if they are all on the same duty cycle, the light is more purple-ish.
  But that can probably be tuned. (At the moment - 2020-07-14 - I'm more busy toying around with
  my new Cyclone-IV EP4CE10E22C8 FPGA board from Omdazz that arrived 2020-07-01.)

- 2020-07-01: Microchip parcel from Thailand arrived with my order of a mixed bunch of PICs to experiment with. Nice!
  Also my 'Omdazz' Cyclone-IV FPGA development board arrived.

- 2020-07-13:

  [Big Clive](https://www.youtube.com/user/bigclivedotcom) replied to a comment from me on one of [his videos
  where he takes apart interesting electronics stuff](https://www.youtube.com/user/bigclivedotcom) and pointed me
  to [Padauk Microcontrollers](http://www.padauk.com.tw/en/product/index.aspx).
  It is always good to learn something new. Many thanks, Clive!

  I completely missed these when I looked for low-pincount MCUs. Looks like Padauk originated around 2005 in Taiwan,
  and their MCUs are much cheaper than the Microchip ones - think 3 cents instead of 53 cents. The also come in an OTP (one-time programmable) variant.
  The instruction set is not documented at bit-level by Padauk, but they were [reverse-engineerd](https://free-pdk.github.io/),
  here is [the Github page](https://github.com/free-pdk).

  From the specification, at least some of the 8-pin variants look very pin-compatible with the Microchip PIC series, with VCC at pin1, GND/VDD at pin8, nRST on pin3, and the rest usable for I/O.
  There is a group of controllers specifically designed for driving RGB LEDs (e.g. the [PMS152](http://www.padauk.com.tw/en/product/show.aspx?num=7&kind=41).
  Many thanks to [Jay Carlson](https://jaycarlson.net/2019/09/06/whats-up-with-these-3-cent-microcontrollers/) for the comment about it.

  ![PMS152 pinout](image/pms152-s08-pinout.png)

  From a comment on one of the review pages for the Padauk controllers it sounds as if it is rather uncommon for them to offer DIP-8 devices, so there is still a small chance the unknown IC is a PIC. I need to read up if/how it is possible to detect a Padauk MCU.

- 2020-07-15: Parcel arrived with my 'Lychee Tang Primer' Anlogic FPGA development kit.

  OK. There are actually a couple more manufacturers than Padauk, some of whom started 'inspired' by PICs (i.e. basically copying a PIC12F508 design, but with OTP instead of Flash).

  At least now I understand the notion of 'one of these ubiquitous 8-bit microcontrollers'. :-)

  Here is a design by [Holtek](https://www.holtek.com/):

  ![HT66F2630 pinout](image/ht66f2630-pinout.png)

  They have quite a bunch of interesting and different pinouts, i.e. VSS amd VDD (GND) on the same side, on the 'top', on the 'bottom'of the row of pins, etc. Interesting stuff!

  And here is a design by [Yspring](http://www.yspringtech.com/):

  ![MDT10F1822 pinout](image/mdt10f1822-pinout.png)

  And a design by [Eastsoft essemi](http://www.essemi.com/):

  ![HR7P179 pinout](image/hr7p179-pinout.png)

  A lot of these manufacturers have tailored their MCUs for a specific purpose, some extending the operation code size, or the stack. Some also reducing the amount of peripherals.

  Unfortunately, the chance that the 'unknown IC' is a PIC12F508 looks now rather small. The main 'drawback' is that I cannot erase the chip and use it for something else, or just re-program it with slightly different software, but will really have to replace it with one of my new shiny PIC12F1572 units. Or any other pin-compatible 8-DIP chip.

  On the bright side, I _can_ use a PIC12F1572 for my own experiments - it works. Great!

  This little project not only made me dabble a bit in PIC microcontrollers, it already showed me a whole new world of interesting hardware. Fascinating!
